'''

This is the main program to be run. It uses all the other sub programs to schedule the synchroniztion
ofthe generator.

'''


import subprocess
import os.path
import csv
import datetime



def read_next_time():
    '''Returns either start/stop as the first argument and the next time to schedule.'''

    with open('./schedules/Loadshedding times.csv', 'r') as csv_file:
        csv_reader = csv.reader(csv_file)

        next(csv_reader)

        for line in csv_reader:
            e = datetime.datetime.now()
            if int(line[0]) == e.day:
                if e.hour < split_time(line[1])[0]:
                    start_stop, day, t = "start", line[0], line[1]
                    break
                elif e.hour == split_time(line[1])[0] and e.minute < split_time(line[1])[1]:
                    start_stop, day, t = "start", line[0], line[1]
                    break
                elif line[2] == "0:30":
                    start_stop, day, t = "stop", str(int(line[0])+1), line[2]
                    break
                elif e.hour < split_time(line[2])[0]:
                    start_stop, day, t = "stop", line[0], line[2]
                    break
                elif e.hour == split_time(line[2])[0] and e.minute < split_time(line[2])[1]:
                    start_stop, day, t = "stop", line[0], line[2]
                    break
            elif int(line[0]) > e.day:
                start_stop, day, t = "start", line[0], line[1]
                break
    return start_stop, day, t


def split_time(t):
    '''Takes in a time string e.g. 12:30 and returns an int array of the hour and the minutes'''
    return [int(t.split(":")[0]), int(t.split(":")[1])]


def add_to_time(day,hour,minute,duration):
    #adds a duration in minutes to time with negative duration equal to subtarion
    dt = datetime.datetime(1,1,day,hour=hour,minute=minute)
    delta = datetime.timedelta(minutes = duration)

    return dt + delta


#Gets the loadshedding status: returns true if currently loadshedding
with subprocess.Popen(["node", ".\eskom-loadshedding-api-master\dist\getStatus.js"], stdout=subprocess.PIPE, shell=True, universal_newlines=True) as proc:
   status = proc.communicate()[0].strip()

if status == "true":
    #Gets the current loadshedding stage
    with subprocess.Popen(["node", ".\eskom-loadshedding-api-master\dist\getStage.js"], stdout=subprocess.PIPE, shell=True, universal_newlines=True) as proc:
        stage = proc.communicate()[0].strip()

    #check for previous stage and populate the new loadshedding times if the stage has changed
    if os.path.exists(".\schedules\previous stage.txt"):
        with open('.\schedules\previous stage.txt', 'r') as stage_txt_file:
            previous_stage = stage_txt_file.read(1)

        if stage != previous_stage:
            with subprocess.Popen(["python", ".\populate schedule.py",stage]) as proc:
                pass

            with open('.\schedules\previous stage.txt', 'w') as writer:
                writer.write(stage)

    else:
        with open('.\schedules\previous stage.txt', 'w') as writer:
            writer.write(stage)

        with subprocess.Popen(["python", ".\populate schedule.py",stage]) as proc:
            pass

    #at this pint we have the correct loadshedding times in the Loadshedding times spreadsheet
    start_stop, day, t = read_next_time()
    t_split = split_time(t)


    if start_stop == "stop":
        genSyncTime = add_to_time(int(day),t_split[0],t_split[1],-30) #30 minutes before time

    else:
        genSyncTime = add_to_time(int(day),t_split[0],t_split[1],-10) #10 minutes before time

    nextRunTime = add_to_time(int(day), t_split[0],t_split[1],5) #5 minutes after time

    e = datetime.datetime.now()
    with subprocess.Popen(["python", ".\Scheduler.py",str(e.year),str(e.month),str(genSyncTime.day),str(genSyncTime.hour),
     str(genSyncTime.minute), "Start stop Generator","Start_stop_generator.py" + " " + start_stop]) as proc:
        pass

    with subprocess.Popen(["python", ".\Scheduler.py",str(e.year),str(e.month),str(nextRunTime.day),str(nextRunTime.hour),
    str(nextRunTime.minute), "Schedule generator sync","run.py"]) as proc:
        pass


else:
    #Not load shedding thus try again in the next hour

    e = datetime.datetime.now()
    delta = datetime.timedelta(minutes = 60)
    e = e + delta

    with subprocess.Popen(["python", ".\Scheduler.py",str(e.year),str(e.month),str(e.day),str(e.hour),
    str(e.minute), "Schedule generator sync","run.py"]) as proc:
        pass
