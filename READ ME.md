This program:
- Picks up the current load shedding stage from eskom using eskom-loadshedding-api-master and populates the next load shedding times using populate schedule.py

- It then uses these times to schedule Start_Stop_generator.py to trigger the generator synchronisation sequence 5 minutes before the next start/stop time. This uses the Windows task scheduler.

- It also schedules itself to run 5 minutes after that time so that it can pick up the next time to schedule.

- It will also have to be configured to run on the start-up of the PC so that you don't have to manually run it ever.

To run the project: "python run.py"