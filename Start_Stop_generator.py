'''

This program takes in either start or stop to initiate the generator start or stop sequences

'''


from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import datetime
import time
import argparse
import smtplib
import email.message
import csv

def LogEvent(event):
    #Adds an event to the logs
    e = datetime.datetime.now()
    data = [e,event]
    with open("./resources/Logs.csv",'a') as csv_file:
        csv_writer = csv.writer(csv_file, delimiter='\t', lineterminator="\n")
        csv_writer.writerow(data)
        csv_file.close()

def email_alert(subject, body, to):
    #This is used to send email notifications
    msg = email.message.EmailMessage()
    msg.set_content(body)
    msg['subject'] = subject
    msg['to'] = to

    with open(r'.\resources\bot_email.txt', 'r') as bot_info_file: #read bot info
        line = bot_info_file.readline()
        user = line.strip().split()[1]
        bot_info_file.readline()
        line = bot_info_file.readline()
        password = line.strip().split()[2]

    msg['from'] = user

    server = smtplib.SMTP("smtp.gmail.com", 587) #gmail server
    server.starttls()
    server.login(user, password)
    server.send_message(msg)

    server.quit()

def send_emails(subject, body):
    #Sends emails to the emails in the EmailsToSendTo.txt file
    with open(r'.\resources\EmailsToSendTo.txt', 'r') as logIn_file: #read the login details
        for line in logIn_file:
            email_alert(subject, body, line.strip())

def logIn(usrN,pwrd):
    #Method to log into the web app
    userName = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="login[username]"]')))
    userName.send_keys(usrN)

    password = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="login[password]"]')))
    password.send_keys(pwrd)

    logInButton = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="login[btnLogin]"]')))
    logInButton.click()
    LogEvent("Log In Attempt")

def startSequence():
    if checkGen() == False:
        time.sleep(10)
        manualButton = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="widgetCanvas"]/div[15]/div/img')))
        manualButton.click()
        LogEvent("Manual Button pressed")
        time.sleep(5)
        if modeState() == "Manual":
            LogEvent("Successful Manual switch")
            startButton = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="widgetCanvas"]/div[19]/div/img')))
            startButton.click()
            LogEvent("Start Button pressed")
            time.sleep(60)

            if checkGen() == True:
                LogEvent("Successful Gen start")
                genBreakerButton = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="widgetCanvas"]/div[21]/div/img')))
                genBreakerButton.click()
                LogEvent("Gen Breaker Button pressed")
                time.sleep(20)

                if checkGenBreaker() == True:
                    LogEvent("Successful Gen Breaker switch")
                    e1 = datetime.datetime.now()
                    time.sleep(5*60) #sleeping for 5 minutes to allow loadshedding to start

                    while checkMains(): #keep delaying the process untill loadshedding starts in case it hasnt after 5 min

                        time.sleep(60)
                        e2 = datetime.datetime.now()
                        delta = e2 - e1
                        if delta.seconds > 30*60 or delta.days > 0: #check if it has been running on manual for more than 30 min
                            subject = "RUNNING ON MANUAL FOR MORE THAN 30 MINUTES!"
                            message = "Generator was put in manual and started! \n\nIt has taken more than 30 minutes for mains to go off. \n\nThe generator will be going back to auto now."
                            send_emails(subject,message)
                            LogEvent(subject)
                            break
                else:
                    subject = "FAILED GEN BREAKER"
                    message = "Gen Breaker symbol did not turn green after 20 seconds of starting the generator \n\nThe generator will be going back to auto now."
                    send_emails(subject,message)
                    LogEvent(subject)

            else:
                subject = "FAILED GEN START"
                message = "Generator available symbol did not turn green after a minute of starting the generator \n\nThe generator will be going back to auto now."
                send_emails(subject,message)
                LogEvent(subject)
        
        else:
            subject = "FAILED MANUAL SWITCH"
            message = "The mode state did not change to manual after the manual button press. \n\nThe generator will be going back to auto now."
            send_emails(subject,message)
            LogEvent(subject)
        
        time.sleep(10)
        autoButton = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="widgetCanvas"]/div[17]/div/img')))
        autoButton.click()
        LogEvent("Auto Button Pressed")
        time.sleep(5)
        
        if modeState() == "Auto":
            LogEvent("Successful Auto switch")
        else:
            subject = "FAILED AUTO SWITCH"
            message = "The mode state did not change to auto after the auto button press."
            send_emails(subject,message)
            LogEvent(subject)

    else: #generator already on
        subject = "START SEQUENCE FAIL!"
        message = "Generator has not been started! \n\nGenerator was not put in manual \n\nThis can happen if the generator is already on."
        send_emails(subject,message)
        LogEvent(subject)

'''
def stopSequence():
    if checkGen() == True:

        #manualButton.click()
        LogEvent("Manual Button pressed")
        print("manual button pressed") #to be removed
        e1 = datetime.datetime.now()
        time.sleep(10*60) #sleeping for 10 minutes to allow loadshedding to stop

        fail = False
        while not checkMains(): #keep delaying the process untill loadshedding stops in case it hasnt after 10 min
            print("mains off") #to be removed

            
            time.sleep(2*60)
            e2 = datetime.datetime.now()
            delta = e2 - e1

            if delta.seconds > 30*60 or delta.days > 0: #check if it has been running on manual for more than 30 min
                fail = True #Failed to capture the return of mains
                subject = "RUNNING ON MANUAL FOR MORE THAN 30 MINUTES!"
                message = "Generator was put in manual and waited for mains to return! \n\nIt has taken more than 30 minutes for mains to come on. \n\nThe generator will be going back to auto now."
                send_emails(subject,message)
                LogEvent(subject)
                break
        if fail:
            pass
        else:
            #startButton.click()
            LogEvent("Stop Button pressed")
            
        #autoButton.click()
        LogEvent("Auto Button pressed")
    
    else: #generator already off
        print("\n\n generator already off \n\n")
        
        subject = "STOP SEQUENCE FAIL!"
        message = "Generator has not been switched off! \n\nGenerator was not put in manual \n\nThis can happen if the generator is already off."
        send_emails(subject,message)
        LogEvent(subject)
'''

def stopSequence():
    time.sleep(10)
    autoButton = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="widgetCanvas"]/div[17]/div/img')))
    autoButton.click()

def checkMains():
    #returns true if the mains is available and false if not by checking the color of the widget 
    driver.refresh()
    time.sleep(10)
    mainsAvailable = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="widgetCanvas"]/div[26]/div/div[1]')))
    s = mainsAvailable.get_attribute('style')   #returns a string that looks like: color: rgb(0, 0, 0); 
    s = s[s.find("(")+1:s.find(")")]
    color = s.split(", ")

    if color[0] == "0" and color[1] == "0" and color[2] == "0":
        return False

    elif color[0] == "57" and color[1] == "212" and color[2] == "31":
        return True

def checkGen():
    #returns true if the generator is running and false if not by checking the color of the widget
    driver.refresh()
    time.sleep(10)
    genAvailable = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="widgetCanvas"]/div[29]/div/div[1]')))
    s = genAvailable.get_attribute('style')   #returns a string that looks like: color: rgb(0, 0, 0); 
    s = s[s.find("(")+1:s.find(")")]
    color = s.split(", ")

    if color[0] == "0" and color[1] == "0" and color[2] == "0":
        return False

    elif color[0] == "57" and color[1] == "212" and color[2] == "31":
        return True

def checkGenBreaker():
    #returns true if the gen Breaker is active and false if not by checking the color of the widget 
    driver.refresh()
    time.sleep(10)
    genBreaker = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="widgetCanvas"]/div[28]/div/div[1]')))
    s = genBreaker.get_attribute('style')   #returns a string that looks like: color: rgb(0, 0, 0); 
    s = s[s.find("(")+1:s.find(")")]
    color = s.split(", ")

    if color[0] == "0" and color[1] == "0" and color[2] == "0":
        return False

    elif color[0] == "57" and color[1] == "212" and color[2] == "31":
        return True

def logOut():
    logOutButton = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '/html/body/div[1]/div[1]/div[2]/a')))
    logOutButton.click()
    LogEvent("Logged Out")

def modeState():
    #returns Auto or Manual
    modeState = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="widgetStateMachine4"]')))
    return modeState.get_attribute('textContent')


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Takes in start or stop to initiate coressponding sequence.')
    #Required start or stop argument
    parser.add_argument('st', help='Either start or stop.')
    args = parser.parse_args()

    subject = args.st.upper() +" SEQUENCE RUNNING"
    message = "The program is running to " + args.st.lower() + " the generator!"
    send_emails(subject,message)

    LogEvent(subject)

    with open(r'.\resources\logInDetails.txt', 'r') as logIn_file: #read the login details
        username = logIn_file.readline()
        password = logIn_file.readline()

    driver = webdriver.Chrome()
    driver.get('https://www.dsewebnet.com/login.php') #This is the gateway portal

    logIn(username.strip(),password.strip())

    gateWayButton = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//*[@id="group_1"]/a/img')))
    gateWayButton.click()

    if args.st.lower() == "start":
        startSequence()

    elif args.st.lower() == "stop":
        stopSequence()

    logOut()
    driver.close()
    LogEvent("END " + args.st.upper() + " SEQUENCE")
