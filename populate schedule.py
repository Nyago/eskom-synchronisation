''' 

This program takes in a loadshedding stage number as an input argument
and populates the loadshedding times for the whole month

'''



import csv
import argparse

#Instantiation of the parser
parser = argparse.ArgumentParser(description='Takes in a loadshedding stage and creates a spreadsheet with the loadshedding times.')

#Required loadshedding stage argument
parser.add_argument('stage', type=int, help='an integer that\'s the current loadshedding stage.')

args = parser.parse_args()

current_stage = args.stage

start_stop = [] #array of start and stop times
daily_schedule = [] #array of each days schedule with position 0 = 1st

schedule = [] #array to contain dictionaries of each dictionary possessinf a data, start and stop time

for i in range(31):
    daily_schedule.append([])

#reading the csv file
with open('./schedules/stellenbosch schedule.csv', 'r') as csv_file:
    csv_reader = csv.reader(csv_file)

    next(csv_reader)
    next(csv_reader)

    rows = 1
    for line in csv_reader:
        if rows > 12: #reading the 12 rows with the required information
            break
        else:
            for i in range(31):
                daily_schedule[i].append(line[i+10]) #populating a 2D array of daily schedules

            start_stop.append([line[8] , line[9] ]) #populating a 2D array of start stop times

            rows += 1

for i, val in enumerate(daily_schedule):

    for k in range(12): #12 represents the 12 rows of data
        if int(val[k]) <= current_stage and int(val[k]) > 0: #if loadshedding
            schedule.append({'Date': i+1, 'Start time': start_stop[k][0], 'Stop time': start_stop[k][1]})

with open('./schedules/Loadshedding times.csv', 'w') as new_file:
    fieldnames = ['Date', 'Start time', 'Stop time']

    csv_writer = csv.DictWriter(new_file,fieldnames=fieldnames, lineterminator="\n")

    csv_writer.writeheader()
    csv_writer.writerows(schedule)
