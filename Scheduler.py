'''

This program takes in a time, date and program to be executed
and schedules in the windows task scheduler

'''


import datetime as dt
import win32com.client
import subprocess
import os
import argparse


def get_parser():
    #Parse inputs
    parser = argparse.ArgumentParser(description='Takes in date time and name of file to be executed.')

    #Required start or stop argument
    parser.add_argument('year', type=int, help='Year to be scheduled e.g. 2020')
    parser.add_argument('month', type=int, help='Month to be scheduled e.g. 6')
    parser.add_argument('date', type=int, help='Date to be scheduled e.g. 28')
    parser.add_argument('hour', type=int, help='Hour to be scheduled e.g. 18')
    parser.add_argument('minute', type=int, help='Minute to be scheduled e.g. 53')
    parser.add_argument('task_name', help='Provide task name')
    parser.add_argument('file', help='Python file to be executed at that time e.g. file.py')
    return parser

args = get_parser().parse_args()



#current working directory
cwd = os.getcwd()

#capture python executable file location
with subprocess.Popen(["where", "python"], stdout=subprocess.PIPE, shell=True, universal_newlines=True) as proc:
   python_location = proc.communicate()[0].strip()

python_location = python_location.split("\n")[0]



#Connection to Task Scheduler
task = win32com.client.Dispatch('Schedule.Service')
task.Connect()
root_folder = task.GetFolder('\\')
newtask = task.NewTask(0)


# Trigger
set_time=dt.datetime(args.year,args.month,args.date,args.hour,args.minute,0,500000)
TASK_TRIGGER_TIME = 1
trigger = newtask.Triggers.Create(TASK_TRIGGER_TIME)
trigger.StartBoundary = set_time.isoformat()


# Action
TASK_ACTION_EXEC = 0
action = newtask.Actions.Create(TASK_ACTION_EXEC)
action.ID = 'TEST'
action.Path = python_location
action.Arguments = args.file
action.WorkingDirectory = cwd

# Parameters
newtask.RegistrationInfo.Description = 'Python Task'
newtask.Settings.Enabled = True
newtask.Settings.StopIfGoingOnBatteries = False

# Saving
TASK_CREATE_OR_UPDATE = 6
TASK_LOGON_NONE = 0
root_folder.RegisterTaskDefinition(
    args.task_name,  # Task name
    newtask,
    TASK_CREATE_OR_UPDATE,
    '',  # No user
    '',  # No password
    TASK_LOGON_NONE)